from binary_field_arithmetic import binary_to_polynomial


def m2abv(x: int, y: list, m: int) -> list:
    """
    Función que realizan la operación AND entre un bit y un vector de bits.

    :param x: bit
    :param y: vector de bits
    :param m: tamaño del campo
    :return: a AND y
    """
    poly_vector = list()
    for i in range(m):
        poly_vector.append(x & y[i])
    return poly_vector


def m2xvv(x: list, y: list, m: int) -> list:
    """
    Función que realizan las operación XOR bit a bit entre dos vectores.

    :param x: lista de digitos binarios
    :param y: lista de digitos binarios
    :param m: tamaño del campo
    :return: x XOR y
    """
    poly_vector = list()
    for i in range(m):
        poly_vector.append(x[i] ^ y[i])
    return poly_vector


def product_alpha_a(a: list, f: list, m: int):
    """
    Función que realiza el calculo de *xa(x) mod f(x)*.

    :param a: coeficientes del elemento del campo
    :param f: coeficientes del polinomio irreducible
    :param m: tamaño del campo
    :return: x * a(x) mod f(x)
    """
    poly_vector = list()
    for i in range(m):
        if i == 0:
            poly_vector.append(a[m-1] & f[0])  # d_0 = a_m−1 * f_0
        else:
            poly_vector.append(a[i-1] ^ (a[m-1] & f[i]))  # d_i = a_i-1 + a_m-1 * f_i
    return poly_vector


def lsb_first_squaring(a: list, f: list):
    """
    Funcion que calcula el Algorithm 7.10—LSB-first squaring de la referencia: HardwareImplementation of Finite-Field
    Arithmetic - Jean-Pierre Deschamps, José Luis Imaña, Gustavo D. Sutter

    :param a: coeficientes de un elemento del campo GF(2^m)
    :param f: coeficientes del polinomio irreducible
    :return: cuadrado del elemento, c(x) = a(x)^2 mod f(x)
    """
    m = len(f) - 1
    # Se forma una lista ascendente con los coefcientes de a(x), a = [a_0, a_1, ... , a_m-1]
    # Se forma una lista ascendente con los coefcientes de f(x), f = [f_0, f_1, ... , f_m-1]
    a_ascending = a[::-1]
    f_ascending = f[::-1]
    # print(f'list_coefficients_of_a = {a_ascending}')
    # print(f'list_coefficients_of_f = {f_ascending}')
    # Se forma una lista ascendente con los coefcientes de c(x), c = [c_0, c_1, ... , c_m-1]
    list_coefficients_of_c = [0] * m

    aux = a_ascending
    # Algorithm 7.10—LSB-first squaring
    for i in range(m):
        # c := m2xvv(m2abv(aux(i),a),c);
        list_coefficients_of_c = m2xvv(m2abv(aux[i], a_ascending, m), list_coefficients_of_c, m)
        a_ascending = product_alpha_a(a_ascending, f_ascending, m)
    # print(f'list_coefficients_of_c = {list_coefficients_of_c[::-1]}')

    return list_coefficients_of_c  # [c0, c1,...,cm-1]


if __name__ == '__main__':
    f_test = [1, 0, 0, 0, 1, 1, 1, 0, 1]  # f(x) = x^8 + x^4 + x^3 + x^2 + 1 -- 100011101
    a_test = [1, 0, 1, 0, 1, 0, 1, 1]  # a(x) =  x^7 + x^5 + x^3 + x^1 + 1
    test_squaring = lsb_first_squaring(a_test, f_test)
    # print(test_squaring)
    print(f"a(x) = {binary_to_polynomial(list(''.join(str(i) for i in a_test)))}")
    print(f"f(x) = {binary_to_polynomial(list(''.join(str(i) for i in f_test)))}")
    print(f"pol_squaring = {binary_to_polynomial(list(''.join(str(i) for i in test_squaring[::-1])))}")
