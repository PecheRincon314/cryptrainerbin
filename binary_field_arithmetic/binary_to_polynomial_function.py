def binary_to_polynomial(binary: list) -> str:
    """
    Funcion que recibie una lista con los coeficientes binarios del elemento de un campo y lo representa en su forma
    polinomial.

    :param binary: lista de unos y ceros de tipo str [MSB,...,LSB]
    :return: representacion de un polynomio con coeficientes que estan en la lista
    """
    poly = str()
    for digit in range(len(binary), 0, -1):
        if binary[-digit] == '1':
            if digit == 1:
                poly += f' {digit} +'
            else:
                poly += f' x^{digit - 1} +'

    return poly[1:-1]


if __name__ == '__main__':
    test = binary_to_polynomial(['1', '0', '1', '1', '1', '1', '1'])
    print(test)
