from numpy import ceil, log2

from binary_field_arithmetic import hex_to_binary, str_list_to_int_list, binary_to_polynomial, lsb_first_multiplier, \
    lsb_first_squaring


def binary_or_square_and_multiply_exponentiation(a: list, e: list, f: list):
    """
    Función que implementa la exponenciacón de acuerdo con el algoritmo 7.16 **Binary or square-and-multiply exponentiation**
    del libro  Hardware Implementation of Finite-Field Arithmetic de Jean-Pierre Deschamps, José Luis Imaña, Gustavo D. Sutter.
    a y m deben tener el mismo numero de digitos binarios.

    :param a: element of a finite field GF(2^m)
    :param e: integer exponent e can be presented in its binary representation as an m-bit vector as is (e0, e1, ..., em−1)
    :param f: irreducible polyno-mial f(x) of degree m
    :return: b = a^e where b ∈ GF(2^m)
    """
    a = str_list_to_int_list(a)  # [am-1,..., a1, a0]
    e = str_list_to_int_list(e)[::-1]  # [e0, e1,...,em-1]
    f = str_list_to_int_list(f)  # [1, fm-1,..., f1, f0]
    m = len(f) - 1
    b = [0] * m
    c = a
    b[-1] = 1
    for i in range(m):
        if e[i] == 1:
            b = lsb_first_multiplier(b, c, f)[::-1]
        c = lsb_first_squaring(c, f)[::-1]
    return b


if __name__ == '__main__':
    m = 8  # exponentiation in GF(2^8)
    hex_a = 0xAB  # a(x) = x^7 + x^5 + x^3 + x^1 + 1
    hex_e = 0xbc  # e = 188
    hex_f = 0x11D  # f(x) = x^8 + x^4 + x^3 + x^2 + 1 -- 100011101
    print(f'a = {binary_to_polynomial(list(hex_to_binary(hex_a, m)))}')
    print(f'e = {hex_e}')
    print(f'f = {binary_to_polynomial(list(hex_to_binary(hex_f, m + 1)))}')
    test_exp = binary_or_square_and_multiply_exponentiation(list(hex_to_binary(hex_a, m)),
                                                            list(hex_to_binary(hex_e, m)),
                                                            list(hex_to_binary(hex_f, m)))
    print(f"pol_exponentiation = {binary_to_polynomial(list(''.join(str(i) for i in test_exp)))}")

