Aritmética en campos primos
================================

**************
Suma *mod p*
**************

.. automodule:: prime_field_arithmetic.addition_mod_p
   :members:
   :undoc-members:
   :show-inheritance:

****************************
Reducción modular de Barret
****************************

.. automodule:: prime_field_arithmetic.barrett_modular_reduction
   :members:
   :undoc-members:
   :show-inheritance:

****************************
Exponenciación *mod p*
****************************

.. automodule:: prime_field_arithmetic.exponentiation_mod_p
   :members:
   :undoc-members:
   :show-inheritance:

**************************************
Generar número primo pseudoaleatorio
**************************************

.. automodule:: prime_field_arithmetic.generating_random_prime
   :members:
   :undoc-members:
   :show-inheritance:

**********************************************
Inverso *mod p*
**********************************************

.. automodule:: prime_field_arithmetic.inverse_mod_p
   :members:
   :undoc-members:
   :show-inheritance:

**********************************************
Reducción modular de Montgomery
**********************************************

.. automodule:: prime_field_arithmetic.montgomery_reduction
   :members:
   :undoc-members:
   :show-inheritance:

******************************
Suma de presición múltiple
******************************

.. automodule:: prime_field_arithmetic.multi_precision_addition
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Multiplicación de presición múltiple
*************************************

.. automodule:: prime_field_arithmetic.multi_precision_multiplication
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Resta de presición múltiple
*************************************

.. automodule:: prime_field_arithmetic.multi_precision_subtraction
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
División de presición múltiple
*************************************

.. automodule:: prime_field_arithmetic.multiple_precision_division
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Cuadrado de presición múltiple
*************************************

.. automodule:: prime_field_arithmetic.multiple_precision_squaring
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Multiplicación *mod p*
*************************************

.. automodule:: prime_field_arithmetic.multiplication_mod_p
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Representación en base **b**
*************************************

.. automodule:: prime_field_arithmetic.radix_b_representation
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Resta *mod p*
*************************************

.. automodule:: prime_field_arithmetic.subtraction_mod_p
   :members:
   :undoc-members:
   :show-inheritance:
