Aritmética en campos binarios
================================

*************************************
Suma *mod f(x)*
*************************************

.. automodule:: binary_field_arithmetic.addition
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Conversor de binario a polinomio
*************************************

.. automodule:: binary_field_arithmetic.binary_to_polynomial_function
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Exponenciación *mod f(x)*
*************************************

.. automodule:: binary_field_arithmetic.exponentiation
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Conversor de binario a hexadecimal
*************************************

.. automodule:: binary_field_arithmetic.hex_to_binary_function
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Generador de la matriz de Mastrovito
*************************************

.. automodule:: binary_field_arithmetic.mastrovito_multiplication_generator
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Multiplicación *mod f(x)*
*************************************

.. automodule:: binary_field_arithmetic.multiplication
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Cuadrado *mod f(x)*
*************************************

.. automodule:: binary_field_arithmetic.squaring
   :members:
   :undoc-members:
   :show-inheritance:

*************************************
Conversor lista str a lista int
*************************************

.. automodule:: binary_field_arithmetic.str_list_to_int_list_function
   :members:
   :undoc-members:
   :show-inheritance:
